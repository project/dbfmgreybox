/* Greybox Redux
 * Required: http://jquery.com/
 * Written by: John Resig
 * Based on code by: 4mir Salihefendic (http://amix.dk)
 * License: LGPL (read more in LGPL.txt)
 * Modified by Geoff Eagles for use with dbFM
 */

var GB_DONE = false;
var GB_HEIGHT = 400;
var GB_WIDTH = 400;
var GB_ANIMATION = false;
var GB_REFRESHFM = 0;          //possible values: 0 = no refresh, 1 = refresh file browser, 2 = refresh file and tree browser

//a new parameter has been added allowing you to specify whether the dbFM browser should be refreshed
function GB_show(caption, url, height, width, refreshit) {
  var closepath;
  
  GB_HEIGHT = height || 400;
  GB_WIDTH = width || 400;
  GB_REFRESHFM = refreshit;
  if(!GB_DONE) {
    closepath = getGreyPath() + '/close.gif';
    $(document.body)
      .append("<div id='GB_overlay'></div><div id='GB_window'><div id='GB_caption'></div>"
        + "<img src='" + closepath + "' id='closeme' alt='Close window'/></div>");

    $("#GB_window img").click(GB_hide);
    $("#GB_overlay").click(GB_hide);
    $(window).resize(GB_position);
    $(window).scroll(GB_position);
    GB_DONE = true;
  }

  $("#GB_frame").remove();
  $("#GB_window").append("<iframe id='GB_frame' src='"+url+"'></iframe>");

  $("#GB_caption").html(caption);
  $("#GB_overlay").show();
  GB_position();

  if(GB_ANIMATION)
    $("#GB_window").slideDown("slow");
  else
    $("#GB_window").show();
}


function GB_hide() {
//need some code here to allow us to decide whether we're refreshing one or both lists.
var retval;
  retval = dbFM.dirListObj;
  if (retval == null) {
    $("#GB_window,#GB_overlay").hide();
  }

  if (GB_REFRESHFM > 0) {
    //this is here to refresh the browser - the only difficulty here is with replication
    //it takes a while for the database to be updated and we don't want to refresh until that's happened
    //ZZZZZZZZ the delay should really be moved to the greybox config options
    if (dowereplicate() == 'T') {
      setTimeout(refreshit(),500);
    }
    else {
      refreshit();
    }
  }
  else {
    $("#GB_window,#GB_overlay").hide();
  }
}

function refreshit(){

  //ok, we've got a problem here - if the greybox has been called from dbfm then we need to refresh the dbfm browser BUT
  //the properties box may have been called from an attachment so there will be no browser to refresh...
  try {
    dbFM.dirListObj.refresh();
    if (GB_REFRESHFM > 1) {
      dbFM.dirTreeObj.fetch();
    }
  }
  catch (err) {
    //don't actually need to worry - if it couldn't do it then it doesn't matter
  }
  $("#GB_window,#GB_overlay").hide();

  return;
}

function GB_position()
{
    var de = document.documentElement;
    var h = self.innerHeight || (de&&de.clientHeight) || document.body.clientHeight;
    var w = self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
    var iebody=(document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body;
    var dsocleft=document.all? iebody.scrollLeft : pageXOffset;
    var dsoctop=document.all? iebody.scrollTop : pageYOffset;
    
    var height = h < GB_HEIGHT ? h - 32 : GB_HEIGHT;
    var top = (h - height)/2 + dsoctop;
    
    $("#GB_window").css({width:GB_WIDTH+"px",height:height+"px",
      left: ((w - GB_WIDTH)/2)+"px", top: top+"px" });
    $("#GB_frame").css("height",height - 32 +"px");
    $("#GB_overlay").css({height:h, top:dsoctop + "px", width:w});
}
